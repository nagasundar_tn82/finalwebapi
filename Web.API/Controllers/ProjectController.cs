﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using ModelLayer;
using System.Web.Http.Cors;
using BusinessLayer;

namespace Web.API.Controllers
{
    //[EnableCors(origins: "*", methods: "*", headers: "*")]
    //[Route("api/ProjectController")]
    [Route("api/Project")]
    public class ProjectController : ApiController
    {


        [HttpGet]
        [Route("GetAllProjectDetails")]
        public IHttpActionResult GetAllProjectDetails()
        {
            BusinessLayer.ProjectBiz bl = new BusinessLayer.ProjectBiz();
            List<ProjectModel> allProjects = bl.GetAllProjectDetails();
            return Ok(allProjects);
        }




        [HttpGet]
        [Route("GetAllProjectTaskDetails")]


        
        public IHttpActionResult GetAllProjectTaskDetails()
        {

            List<ProjectModel> lstProject = new List<ProjectModel>();

            List<ProjectTaskDetails> allProjects = new List<ProjectTaskDetails>();
            List<TaskModel> projectTasks = new List<TaskModel>();
            List<UserModel> projectUsers = new List<UserModel>();

            BusinessLayer.ProjectBiz bl = new BusinessLayer.ProjectBiz();
            TaskBiz tBiz = new TaskBiz();
            UserBiz uBiz = new UserBiz();
            lstProject = bl.GetAllProjectDetails();
            projectUsers = uBiz.GetAllUserDetails();


            foreach (ProjectModel proj in lstProject)
            {
                ProjectTaskDetails newProj = new ProjectTaskDetails();
                newProj.Project_ID = proj.PROJECT_ID;
                newProj.Project_Name = proj.PROJECT;
                newProj.PROJECT = proj.PROJECT;
                newProj.Start_Date = proj.START_DATE;
                newProj.End_Date = proj.END_DATE;
                newProj.Priority = proj.PRIORITY;

                projectTasks = tBiz.GetTasksByProjectID(proj.PROJECT_ID);

                if(projectTasks != null && projectTasks.Count > 0)
                {
                    newProj.NoOfTasks = projectTasks.Count;
                    newProj.NoOfTasksCompleted = projectTasks.Where(a => a.STATUS == 1).Count();
                }
                if (projectUsers != null && projectUsers.Count > 0)
                {
                    newProj.Manager = projectUsers.Where(a => a.PROJECT_ID == proj.PROJECT_ID).FirstOrDefault();
                }
                allProjects.Add(newProj);

            }


            return Ok(allProjects);


        }
        [HttpGet]
        [Route("GetSearchProjectID")]
       
        public IHttpActionResult GetSearchProjectID(int ProjectID)
        {
 
            ProjectModel clickedProject = new ProjectModel();
            BusinessLayer.ProjectBiz bl = new BusinessLayer.ProjectBiz();
            clickedProject = bl.GetSearchProjectID(ProjectID);
            return Ok(clickedProject);
        }

        [HttpPost]
        [Route("AddProjectDetails")]
        public IHttpActionResult AddProjectDetails(ProjectModel projectDetails)
           // public IHttpActionResult AddProjectDetails()
        {
            //ProjectModel projectDetails = new ProjectModel();
            //projectDetails.END_DATE = "01/01/2012";
            //projectDetails.PRIORITY = 1;
            //projectDetails.PROJECT = "AGI -API";
            //projectDetails.PROJECT_ID = 2;
            //projectDetails.START_DATE = "01/01/2012";

            BusinessLayer.ProjectBiz bl = new BusinessLayer.ProjectBiz();
            bl.AddProjectDetails(projectDetails);
            return Ok(projectDetails);
        }


        [HttpPut]
        [Route("UpdateProjectDetails")]
        public IHttpActionResult UpdateProjectDetails(ProjectModel projectDetails)
        {             
            BusinessLayer.ProjectBiz bl = new BusinessLayer.ProjectBiz();
            bl.UpdateProjectDetails(projectDetails);
            return Ok(projectDetails);
        }


        [HttpDelete]
        [Route("DeleteProjectDetails")]
        public IHttpActionResult DeleteProjectDetails(int projectID)         
        {           
            BusinessLayer.ProjectBiz bl = new BusinessLayer.ProjectBiz();
            bool retValue = bl.DeleteProjectDetails(projectID);
            return Ok(retValue);            
        }

       
    }
}
