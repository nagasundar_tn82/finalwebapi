﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using ModelLayer;
using System.Web.Http.Cors;

namespace Web.API.Controllers
{
    //[EnableCors(origins: "*", methods: "*", headers: "*")]
    [Route("api/User")]  
    public class UserController : ApiController
    {
        
        [HttpGet]
        [Route("GetSearchUserIDByTaskId")]
        public IHttpActionResult GetSearchUserIDByTaskId(int taskID)
        {
 
            UserModel clickedUser = new UserModel();
            BusinessLayer.UserBiz bl = new BusinessLayer.UserBiz();
            clickedUser = bl.GetSearchUserIDByTaskId(taskID);



            return Ok(clickedUser);

        }

        [HttpPost]
        [Route("AddUserDetails")]
        public IHttpActionResult AddUserDetails(UserModel userDetails)
        {



            BusinessLayer.UserBiz bl = new BusinessLayer.UserBiz();
            bl.AddUserDetails(userDetails);

            return Ok(userDetails);
        }


        [HttpPut]
        [Route("UpdateUserDetails")]
        public IHttpActionResult UpdateUserDetails(UserModel userDetails)
        {
             


            BusinessLayer.UserBiz bl = new BusinessLayer.UserBiz();
            bl.UpdateUserDetails(userDetails);
            return Ok(userDetails);
        }


        [HttpDelete]
        [Route("DeleteUserDetails")]
        public IHttpActionResult DeleteUserDetails(int userID)
         
        {
           


            BusinessLayer.UserBiz bl = new BusinessLayer.UserBiz();
            bool retValue = bl.DeleteUserDetails(userID);


            return Ok(retValue);

            


        }


        [HttpGet]
        [Route("GetAllUserDetails")]
        public IHttpActionResult GetAllUserDetails()
        {

            List<UserModel> lstUser = new List<UserModel>();

            BusinessLayer.UserBiz bl = new BusinessLayer.UserBiz();
            lstUser = bl.GetAllUserDetails();

        
            return Ok(lstUser);


        }

        
               [HttpPut]
        [Route("UpdateUserOnEditTask")]
        public IHttpActionResult UpdateUserOnEditTask(UserModel userDetails)
        {



            BusinessLayer.UserBiz bl = new BusinessLayer.UserBiz();
            bl.UpdateUserOnEditTask(userDetails);
            return Ok(userDetails);
        }
    }
}
