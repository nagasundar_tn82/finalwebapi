﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using ModelLayer;
using System.Web.Http.Cors;

namespace Web.API.Controllers
{
    //[EnableCors(origins: "*", methods: "*", headers: "*")]
    [Route("api/Task")]
    public class TaskController : ApiController
    {

     

        [HttpPost]
        [Route("AddTaskDetails")]


        
        
        public IHttpActionResult AddTaskDetails(TaskModel taskDetails)
        {



            BusinessLayer.TaskBiz bl = new BusinessLayer.TaskBiz();
            bl.AddTaskDetails(taskDetails);

            return Ok(taskDetails);
        }


        [HttpPut]
        [Route("UpdateTaskDetails")]
        public IHttpActionResult UpdateTaskDetails(TaskModel taskDetails)
        {
             


            BusinessLayer.TaskBiz bl = new BusinessLayer.TaskBiz();
            bl.UpdateTaskDetails(taskDetails);
            return Ok(taskDetails);
        }


        [HttpDelete]
        [Route("DeleteTaskDetails")]
        public IHttpActionResult DeleteTaskDetails(int taskID)
         
        {
           


            BusinessLayer.TaskBiz bl = new BusinessLayer.TaskBiz();
            bool retValue = bl.DeleteTaskDetails(taskID);


            return Ok(retValue);

            


        }


        [HttpGet]
        [Route("GetAllTaskDetails")]
        public IHttpActionResult GetAllTaskDetails()
        {

            List<TaskModel> lstTask = new List<TaskModel>();

            BusinessLayer.TaskBiz bl = new BusinessLayer.TaskBiz();
            lstTask = bl.GetAllTaskDetails();

        
            return Ok(lstTask);


        }

        [HttpGet]
        [Route("GetTasksByProjectID")]
        public IHttpActionResult GetTasksByProjectID(int projectID)
        {

            List<TaskModel> lstTask = new List<TaskModel>();

            BusinessLayer.TaskBiz bl = new BusinessLayer.TaskBiz();
            lstTask = bl.GetTasksByProjectID(projectID);


            return Ok(lstTask);


        }
        [HttpGet]
        [Route("GetSearchTaskID")]

        public IHttpActionResult GetSearchTaskID(int TaskID)
        {

            TaskModel clickedTask = new TaskModel();
            BusinessLayer.TaskBiz bl = new BusinessLayer.TaskBiz();
            clickedTask = bl.GetSearchTaskID(TaskID);



            return Ok(clickedTask);

        }

        [HttpGet]
        [Route("GetAllParentTasks")]
        public IHttpActionResult GetAllParentTasks()
        {

            List<ParentTaskModel> lstparentTask = new List<ParentTaskModel>();            
            BusinessLayer.TaskBiz bl = new BusinessLayer.TaskBiz();
            lstparentTask = bl.GetAllParentTasks(); 
            return Ok(lstparentTask);
        }

        [HttpPost]
        [Route("AddParentTasksDetail")]
        public IHttpActionResult AddParentTasksDetail(ParentTaskModel newParentTask)
        {   
            BusinessLayer.TaskBiz bl = new BusinessLayer.TaskBiz();
            bl.AddParentTasksDetail(newParentTask);
            return Ok(newParentTask);
        }
        [HttpGet]
        [Route("GetParentName")]

        public IHttpActionResult GetParentName(int parentID)
        {

            ParentTaskModel clickedTask = new ParentTaskModel();
            BusinessLayer.TaskBiz bl = new BusinessLayer.TaskBiz();
            clickedTask = bl.GetParentName(parentID);



            return Ok(clickedTask);

        }



    }
}
