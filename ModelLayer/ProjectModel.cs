﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace ModelLayer
{
    [Table("PROJECT_TABLE")]
    public class ProjectModel
    {
        [Key]
        public int PROJECT_ID { get; set; }       
        public string PROJECT { get; set; }                  
        public string START_DATE { get; set; } 
        public string END_DATE { get; set; }
        public int PRIORITY { get; set; }   
    }
}
