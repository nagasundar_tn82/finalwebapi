﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace ModelLayer
{
    [Table("TASK_TABLE")]
    public class TaskModel
    {
        [Key]        
        public int TASK_ID { get; set; }
        public int PARENT_ID { get; set; }
        public int PROJECT_ID { get; set; }
        public string TASK { get; set; }                  
        public string START_DATE { get; set; } 
        public string END_DATE { get; set; }
        public int PRIORITY { get; set; }
        public int STATUS { get; set; }

    }
}
