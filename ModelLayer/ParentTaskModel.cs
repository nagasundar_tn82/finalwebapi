﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ModelLayer
{
    [Table("PARENTTASK_TABLE")]
    public class ParentTaskModel
    {
        [Key]
        public int PARENT_ID { get; set; }
        public string PARENT_TASK { get; set; }
    }
}
