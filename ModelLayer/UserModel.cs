﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace ModelLayer
{
    [Table("USERS_TABLE")]
    public class UserModel
    {
        [Key]
        public int USER_ID { get; set; }             
        public string FIRST_NAME { get; set; } 
        public string LAST_NAME { get; set; }
        public int EMPLOYEE_ID { get; set; }
        public int PROJECT_ID { get; set; }
        public int TASK_ID { get; set; }
    }
}
