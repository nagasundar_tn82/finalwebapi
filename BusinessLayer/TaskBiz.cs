﻿using DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelLayer;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BusinessLayer
{
    public class TaskBiz
    {
        public List<TaskModel> GetAllTaskDetails()
        {             
            List<TaskModel> taskList = new List<TaskModel>();
            using (DALayer dataAccessLayerInstance = new DALayer())
            {
                return dataAccessLayerInstance.Task.ToList();
            }

        }

     


        public void AddTaskDetails(TaskModel newTask)
        {
            using (DALayer daLayerObject = new DALayer())
            {
                // TaskB getParenet = new ParentTaskModel();
                //if (newTask.PARENT_ID > 0)
                //{
                //    ParentTaskModel newParentTask = new ParentTaskModel();
                //    newParentTask.PARENT_TASK = newTask.TASK;
                //    daLayerObject.ParentTask.Add(newParentTask);
                //    daLayerObject.SaveChanges();

                //    newParentTask.PARENT_ID = daLayerObject.ParentTask.FirstOrDefault(a => a.PARENT_TASK == newTask.TASK).PARENT_ID;
                //    //GetParentID(newTask.TASK);
                //    newTask.PARENT_ID = newParentTask.PARENT_ID;


       
                //}
                daLayerObject.Task.Add(newTask);
                daLayerObject.SaveChanges();

            }

        }


        public void UpdateTaskDetails(TaskModel taskToUpdate)
        {
            using (DALayer daLayerObject = new DALayer())
            {             
                TaskModel updateTask = daLayerObject.Task.FirstOrDefault(a => a.TASK_ID == taskToUpdate.TASK_ID);               

                if (updateTask != null)
                {                    
                    updateTask.STATUS = taskToUpdate.STATUS;
                    updateTask.PARENT_ID = taskToUpdate.PARENT_ID;
                    updateTask.PRIORITY = taskToUpdate.PRIORITY;
                    updateTask.START_DATE = taskToUpdate.START_DATE;
                    updateTask.TASK = taskToUpdate.TASK;
                    updateTask.END_DATE = taskToUpdate.END_DATE;
                    updateTask.PROJECT_ID = taskToUpdate.PROJECT_ID;
                    daLayerObject.SaveChanges();
                }
                else
                {
                    throw new Exception(string.Format("Task - {0} not found", taskToUpdate.TASK));
                }
            }

        }

        public bool DeleteTaskDetails(int id)
        {
            using (DALayer daLayerObject = new DALayer())
            {
                TaskModel taskToDelete = daLayerObject.Task.FirstOrDefault(a => a.TASK_ID == id);
                if (taskToDelete != null)
                {
                    daLayerObject.Task.Remove(taskToDelete);
                    daLayerObject.SaveChanges();
                    return true;
                }
                else
                {
                    throw new Exception(string.Format("Task - {0} not found", taskToDelete.TASK));
                }
            }

        }

        public List<TaskModel> GetTasksByProjectID(int projectID)
        {

            using (DALayer daLayerObject = new DALayer())
            {
                List<TaskModel> allTasks = (from specificTask in daLayerObject.Task
                                            select specificTask).ToList();
                allTasks = allTasks.Where(a => a.PROJECT_ID == projectID).ToList();
                return allTasks;
            }
        }


        public TaskModel GetSearchTaskID(int TaskID)
        {

            using (DALayer daLayerObject = new DALayer())
            {
                List<TaskModel> allTasks = (from specificTask in daLayerObject.Task
                                            select specificTask).ToList();
                TaskModel task = allTasks.Where(a => a.TASK_ID == TaskID).FirstOrDefault();
                return task;
            }
        }



        public List<ParentTaskModel> GetAllParentTasks()
        {
            List<ParentTaskModel> taskList = new List<ParentTaskModel>();
            using (DALayer dataAccessLayerInstance = new DALayer())
            {
                return dataAccessLayerInstance.ParentTask.ToList();
            }

        }

        public void AddParentTasksDetail(ParentTaskModel newParentTask)
        {
           
            using (DALayer daLayerObject = new DALayer())
            {
                daLayerObject.ParentTask.Add(newParentTask);
                daLayerObject.SaveChanges();
            }
        }

        public ParentTaskModel GetParentName(int parentID)
        {

            using (DALayer daLayerObject = new DALayer())
            {
                List<ParentTaskModel> allTasks = (from specificTask in daLayerObject.ParentTask
                                                  select specificTask).ToList();
                ParentTaskModel task = allTasks.Where(a => a.PARENT_ID == parentID).FirstOrDefault();
                return task;
            }
        }

    }
}