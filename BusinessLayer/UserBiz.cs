﻿using DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelLayer;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BusinessLayer
{
    public class UserBiz
    {
        public List<UserModel> GetAllUserDetails()
        {             
            List<UserModel> taskList = new List<UserModel>();
            using (DALayer dataAccessLayerInstance = new DALayer())
            {
                return dataAccessLayerInstance.User.ToList();
            }

        }

        public UserModel GetSearchUserIDByTaskId(int taskID)
        {

            using (DALayer daLayerObject = new DALayer())
            {
                List<UserModel> allUsers = (from specificUser in daLayerObject.User
                                       select specificUser).ToList();
                UserModel task = allUsers.Where(a => a.TASK_ID == taskID).FirstOrDefault();
                return task;
            }
        }


        public void AddUserDetails(UserModel newUser)
        {
            using (DALayer daLayerObject = new DALayer())
            {
                daLayerObject.User.Add(newUser);
                daLayerObject.SaveChanges();
            }

        }


        public void UpdateUserDetails(UserModel userToUpdate)
        {
            using (DALayer daLayerObject = new DALayer())
            {             
                UserModel updateUser = daLayerObject.User.FirstOrDefault(a => a.USER_ID == userToUpdate.USER_ID);
                
                if (updateUser != null)
                {
                     
                    updateUser.FIRST_NAME = userToUpdate.FIRST_NAME;
                    updateUser.LAST_NAME = userToUpdate.LAST_NAME;
                    updateUser.EMPLOYEE_ID = userToUpdate.EMPLOYEE_ID;
                    updateUser.TASK_ID = userToUpdate.TASK_ID;
                    updateUser.PROJECT_ID = userToUpdate.PROJECT_ID;
                    daLayerObject.SaveChanges();
                }
                else
                {
                    throw new Exception(string.Format("User - {0} not found", userToUpdate.FIRST_NAME));
                }
            }

        }

        public void UpdateUserOnEditTask(UserModel userToUpdate)
        {
            using (DALayer daLayerObject = new DALayer())
            {
                UserModel updateUser = daLayerObject.User.FirstOrDefault(a => a.USER_ID == userToUpdate.USER_ID);

                UserModel oldUser = daLayerObject.User.FirstOrDefault(a => a.TASK_ID == userToUpdate.TASK_ID && (a.TASK_ID != 0));
                if (oldUser != null)
                {
                    oldUser.TASK_ID = 0;
                    daLayerObject.SaveChanges();

                }

                if (updateUser != null)
                {

                    updateUser.FIRST_NAME = userToUpdate.FIRST_NAME;
                    updateUser.LAST_NAME = userToUpdate.LAST_NAME;
                    updateUser.EMPLOYEE_ID = userToUpdate.EMPLOYEE_ID;
                    updateUser.TASK_ID = userToUpdate.TASK_ID;
                    updateUser.PROJECT_ID = userToUpdate.PROJECT_ID;
                    daLayerObject.SaveChanges();
                }
                else
                {
                    throw new Exception(string.Format("User - {0} not found", userToUpdate.FIRST_NAME));
                }
            }
        }


        public bool DeleteUserDetails(int id)
        {
            using (DALayer daLayerObject = new DALayer())
            {
                UserModel userToDelete = daLayerObject.User.FirstOrDefault(a => a.USER_ID == id);
                if (userToDelete != null)
                {
                    daLayerObject.User.Remove(userToDelete);
                    daLayerObject.SaveChanges();
                    return true;
                }
                else
                {
                    throw new Exception(string.Format("User - {0} not found", userToDelete.USER_ID));
                }
            }

        }
    }
}