﻿using DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelLayer;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BusinessLayer
{
    public class ProjectBiz
    {
        public List<ProjectModel> GetAllProjectDetails()
        {             
            List<ProjectModel> ProjectList = new List<ProjectModel>();
            using (DALayer dataAccessLayerInstance = new DALayer())
            {
                  return dataAccessLayerInstance.Project.ToList();                
            }

        }

        public ProjectModel GetSearchProjectID(int ProjectID)
        {

            using (DALayer daLayerObject = new DALayer())
            {
                List<ProjectModel> allProjects = (from specificProject in daLayerObject.Project
                                                  select specificProject).ToList();
                ProjectModel Project = allProjects.Where(a => a.PROJECT_ID == ProjectID).FirstOrDefault();
                return Project;

            }
        } 


        public void AddProjectDetails(ProjectModel newProject)
        {
            using (DALayer daLayerObject = new DALayer())
            {
                daLayerObject.Project.Add(newProject);
                daLayerObject.SaveChanges();
            }

        }


        public void UpdateProjectDetails(ProjectModel ProjectToUpdate)
        {
            using (DALayer daLayerObject = new DALayer())
            {
                ProjectModel updateProject = daLayerObject.Project.FirstOrDefault(a => a.PROJECT_ID == ProjectToUpdate.PROJECT_ID);

                if (updateProject != null)
                {
                    updateProject.PROJECT = ProjectToUpdate.PROJECT;
                    updateProject.START_DATE = ProjectToUpdate.START_DATE;
                    updateProject.END_DATE = ProjectToUpdate.END_DATE;
                    updateProject.PRIORITY = ProjectToUpdate.PRIORITY;
                    updateProject.PROJECT_ID = ProjectToUpdate.PROJECT_ID;
                    daLayerObject.SaveChanges();
                }
                else
                {
                    throw new Exception(string.Format("Project - {0} not found", ProjectToUpdate.PROJECT));
                }
            }

        }

        public bool DeleteProjectDetails(int id)
        {
            using (DALayer daLayerObject = new DALayer())
            {
                ProjectModel ProjectToDelete = daLayerObject.Project.FirstOrDefault(a => a.PROJECT_ID == id);
                if (ProjectToDelete != null)
                {
                    daLayerObject.Project.Remove(ProjectToDelete);
                    daLayerObject.SaveChanges();
                    return true;
                }
                else
                {
                    throw new Exception(string.Format("Project - {0} not found", ProjectToDelete.PROJECT));
                }
            }
            
        }



    }


    public class ProjectTaskDetails
    {
        public int Project_ID { get; set; }
        public string Project_Name { get; set; }
        public string PROJECT { get; set; }
        public string Start_Date { get; set; }
        public string End_Date { get; set; }
        public int? Priority { get; set; }
        public int? NoOfTasks { get; set; }
        public int? NoOfTasksCompleted { get; set; }
        public UserModel Manager { get; set; }
    }
}