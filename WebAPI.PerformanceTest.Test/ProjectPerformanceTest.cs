﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NBench;
using Web.API.Controllers;
using ModelLayer;
using Web.API;
 

namespace WebAPI.PerformanceTest.Test
{
    public class ProjectPerformanceTest
    {
        [PerfBenchmark(Description = "Performace Test for PROJECT GET", RunMode = RunMode.Iterations, TestMode = TestMode.Test)]
        [MemoryAssertion(MemoryMetric.TotalBytesAllocated, MustBe.LessThan, 100000d)]
        [ElapsedTimeAssertion(MaxTimeMilliseconds = 500)]
        [CounterMeasurement("ProjectController")]
      

        public void GetAllProjectDetails_Benchmark_Performance_ElaspedTime()
        {
            ProjectController tc = new ProjectController();
            tc.GetAllProjectDetails();
        }




        [PerfBenchmark(Description = "Performace Test for PROJECT ADD", RunMode = RunMode.Iterations, TestMode = TestMode.Test)]
        [MemoryAssertion(MemoryMetric.TotalBytesAllocated, MustBe.LessThan, 100000d)]
        [ElapsedTimeAssertion(MaxTimeMilliseconds = 500)]
        [CounterMeasurement("ProjectController")]
        public void AddProjectDetails_Benchmark_Performance_ElaspedTime()
        {
            ProjectController controller = new ProjectController();
            ProjectModel projDetails = new ProjectModel();
            projDetails.PROJECT = "Proj1";
            projDetails.PROJECT_ID = 5;
            projDetails.PRIORITY = 4;
            projDetails.START_DATE = "2019-15-4";
            projDetails.END_DATE = "2019-18-5";

            controller.AddProjectDetails(projDetails);
        }

        [PerfBenchmark(Description = "Performace Test for PROJECT UPDATE", RunMode = RunMode.Iterations, TestMode = TestMode.Test)]
        [MemoryAssertion(MemoryMetric.TotalBytesAllocated, MustBe.LessThan, 100000d)]
        [ElapsedTimeAssertion(MaxTimeMilliseconds = 500)]
        [CounterMeasurement("ProjectController")]
        public void UpdateProjectDetails_Benchmark_Performance_ElaspedTime()
        {
            ProjectController controller = new ProjectController();
            ProjectController tc = new ProjectController();
            ProjectModel projDetails = new ProjectModel();
            projDetails.PROJECT = "Proj1";
            projDetails.PROJECT_ID = 1003;
            projDetails.PRIORITY = 4;
            projDetails.START_DATE = "2019-15-4";
            projDetails.END_DATE = "2019-18-5";
            controller.UpdateProjectDetails(projDetails);
        }


        //[PerfBenchmark(Description = "Performace Test for PROJECT DELETE", RunMode = RunMode.Iterations, TestMode = TestMode.Test)]
        //[MemoryAssertion(MemoryMetric.TotalBytesAllocated, MustBe.LessThan, 100000d)]
        //[ElapsedTimeAssertion(MaxTimeMilliseconds = 500)]
        //[CounterMeasurement("ProjectController")]
        //public void DeleteProjectDetails_Benchmark_Performance_ElaspedTime()
        //{
        //    ProjectController controller = new ProjectController();
        //    controller.DeleteProjectDetails(2019);
        //}
    }
}
