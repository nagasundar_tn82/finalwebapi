﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NBench;
using Web.API.Controllers;
using ModelLayer;
using Web.API;

namespace WebAPI.PerformanceTest.Test
{
    public class TaskPerformanceTests
    {
        [PerfBenchmark(Description = "Performace Test for TASK GET", RunMode = RunMode.Iterations, TestMode = TestMode.Test)]
        [MemoryAssertion(MemoryMetric.TotalBytesAllocated, MustBe.LessThan, 100000d)]
        [ElapsedTimeAssertion(MaxTimeMilliseconds = 500)]
        [CounterMeasurement("TaskController")]


        public void GetAllTaskDetails_Benchmark_Performance_ElaspedTime()
        {
            TaskController tc = new TaskController();
            tc.GetAllTaskDetails();
        }




        [PerfBenchmark(Description = "Performace Test for TASK ADD", RunMode = RunMode.Iterations, TestMode = TestMode.Test)]
        [MemoryAssertion(MemoryMetric.TotalBytesAllocated, MustBe.LessThan, 100000d)]
        [ElapsedTimeAssertion(MaxTimeMilliseconds = 500)]
        [CounterMeasurement("TaskController")]
        public void AddTaskDetails_Benchmark_Performance_ElaspedTime()
        {
            TaskController controller = new TaskController();
           
            TaskModel taskDetails = new TaskModel();
            taskDetails.TASK = "TestingUnit";
            taskDetails.TASK_ID = 1;
            taskDetails.PARENT_ID = 1;
            taskDetails.PROJECT_ID = 1;
            taskDetails.START_DATE = "2019-15-4";
            taskDetails.END_DATE = "2019-18-5";
            taskDetails.PRIORITY = 1;
            taskDetails.STATUS = 1;
           

            controller.AddTaskDetails(taskDetails);
        }

        [PerfBenchmark(Description = "Performace Test for TASK UPDATE", RunMode = RunMode.Iterations, TestMode = TestMode.Test)]
        [MemoryAssertion(MemoryMetric.TotalBytesAllocated, MustBe.LessThan, 100000d)]
        [ElapsedTimeAssertion(MaxTimeMilliseconds = 500)]
        [CounterMeasurement("TaskController")]
        public void UpdateTaskDetails_Benchmark_Performance_ElaspedTime()
        {
            TaskController controller = new TaskController();
            TaskController tc = new TaskController();
            TaskModel taskDetails = new TaskModel();
            taskDetails.TASK = "TestingUnit";
            taskDetails.TASK_ID = 1;
            taskDetails.PARENT_ID = 1;
            taskDetails.PROJECT_ID = 1;
            taskDetails.START_DATE = "2019-15-4";
            taskDetails.END_DATE = "2019-18-5";
            taskDetails.PRIORITY = 1;
            taskDetails.STATUS = 1;
            controller.UpdateTaskDetails(taskDetails);
        }


        //[PerfBenchmark(Description = "Performace Test for TASK DELETE", RunMode = RunMode.Iterations, TestMode = TestMode.Test)]
        //[MemoryAssertion(MemoryMetric.TotalBytesAllocated, MustBe.LessThan, 100000d)]
        //[ElapsedTimeAssertion(MaxTimeMilliseconds = 500)]
        //[CounterMeasurement("TaskController")]
        //public void DeleteTaskDetails_Benchmark_Performance_ElaspedTime()
        //{
        //    TaskController controller = new TaskController();
        //    controller.DeleteTaskDetails(2019);
        //}
    }
}
