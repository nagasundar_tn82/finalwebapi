﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NBench;
using Web.API.Controllers;
using ModelLayer;
using Web.API;

namespace WebAPI.PerformanceTest.Test
{
    class UserPerformanceTest
    {
        [PerfBenchmark(Description = "Performace Test for USER GET", RunMode = RunMode.Iterations, TestMode = TestMode.Test)]
        [MemoryAssertion(MemoryMetric.TotalBytesAllocated, MustBe.LessThan, 100000d)]
        [ElapsedTimeAssertion(MaxTimeMilliseconds = 500)]
        [CounterMeasurement("UserController")]


        public void GetAllUserDetails_Benchmark_Performance_ElaspedTime()
        {
            UserController tc = new UserController();
            tc.GetAllUserDetails();
        }




        [PerfBenchmark(Description = "Performace Test for USER ADD", RunMode = RunMode.Iterations, TestMode = TestMode.Test)]
        [MemoryAssertion(MemoryMetric.TotalBytesAllocated, MustBe.LessThan, 100000d)]
        [ElapsedTimeAssertion(MaxTimeMilliseconds = 500)]
        [CounterMeasurement("UserController")]
        public void AddUserDetails_Benchmark_Performance_ElaspedTime()
        {
            UserController controller = new UserController();

            UserModel UserDetails = new UserModel();
            UserDetails.FIRST_NAME = "User1";
            UserDetails.EMPLOYEE_ID = 5;
            UserDetails.PROJECT_ID = 4;
            UserDetails.LAST_NAME = "LastName1";
            UserDetails.TASK_ID = 14;
            UserDetails.USER_ID = 1;
            controller.UpdateUserDetails(UserDetails);

            controller.AddUserDetails(UserDetails);
        }

        [PerfBenchmark(Description = "Performace Test for USER UPDATE", RunMode = RunMode.Iterations, TestMode = TestMode.Test)]
        [MemoryAssertion(MemoryMetric.TotalBytesAllocated, MustBe.LessThan, 100000d)]
        [ElapsedTimeAssertion(MaxTimeMilliseconds = 500)]
        [CounterMeasurement("UserController")]
        public void UpdateUserDetails_Benchmark_Performance_ElaspedTime()
        {
            UserController controller = new UserController();
            UserController tc = new UserController();
            UserModel UserDetails = new UserModel();
            UserDetails.FIRST_NAME = "User1";
            UserDetails.EMPLOYEE_ID = 5;
            UserDetails.PROJECT_ID = 4;
            UserDetails.LAST_NAME = "LastName1";
            UserDetails.TASK_ID = 14;
            UserDetails.USER_ID = 1;
            controller.UpdateUserDetails(UserDetails);
        }


        //[PerfBenchmark(Description = "Performace Test for USER DELETE", RunMode = RunMode.Iterations, TestMode = TestMode.Test)]
        //[MemoryAssertion(MemoryMetric.TotalBytesAllocated, MustBe.LessThan, 100000d)]
        //[ElapsedTimeAssertion(MaxTimeMilliseconds = 500)]
        //[CounterMeasurement("UserController")]
        //public void DeleteUserDetails_Benchmark_Performance_ElaspedTime()
        //{
        //    UserController controller = new UserController();
        //    controller.DeleteUserDetails(2019);
        //}
    }
}
