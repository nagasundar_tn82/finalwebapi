﻿using System;
using Web.API.Controllers;
using ModelLayer;
using System.Web.Http.Results;
using System.Collections.Generic;
using System.Web.Http;

using NUnit.Framework;

namespace WebAPITest
{
    [TestFixture]
    public class TaskTest
    {
        [Test]
        public void GetAllTaskDetails_Test()
        {
            TaskController tc = new  TaskController();
            var result = tc.GetAllTaskDetails();
            var actual = result as OkNegotiatedContentResult<List<TaskModel>>;
            Assert.IsNotNull(actual);
            Assert.IsNotNull(actual.Content);
        }

        [Test]
        public void AddTaskDetails_Test()
        {
            TaskModel taskDetails = new TaskModel();
            taskDetails.TASK = "TestingUnit";
            taskDetails.TASK_ID = 1;
            taskDetails.PARENT_ID = 1;
            taskDetails.PROJECT_ID = 1;
            taskDetails.START_DATE = "2019-15-4";
            taskDetails.END_DATE = "2019-18-5";
            taskDetails.PRIORITY = 1;
            taskDetails.STATUS = 1;                           

            TaskController tc = new TaskController();

            IHttpActionResult result = tc.AddTaskDetails(taskDetails);
            var actual = result as OkNegotiatedContentResult<TaskModel>;
            Assert.IsNotNull(actual);
            Assert.IsNotNull(actual.Content);
        }

        [Test]
        public void UpdateTaskDetails_Test()
        {
            TaskController tc = new TaskController();
            TaskModel taskDetails = new TaskModel();
            taskDetails.TASK = "TestingUnit";
            taskDetails.TASK_ID = 1;
            taskDetails.PARENT_ID = 1;
            taskDetails.PROJECT_ID = 1;
            taskDetails.START_DATE = "2019-15-4";
            taskDetails.END_DATE = "2019-18-5";
            taskDetails.PRIORITY = 1;
            taskDetails.STATUS = 1;
            var result = tc.UpdateTaskDetails(taskDetails);
            var actual = result as OkNegotiatedContentResult<TaskModel>;
            Assert.IsNotNull(actual);

        }

        [Test]
        public void DeleteTaskDetails_Test()
        {
            TaskController tc = new TaskController();
            var result = tc.DeleteTaskDetails(1024);
            var actual = result as OkNegotiatedContentResult<bool>;
            Assert.IsNotNull(actual);
            Assert.IsNotNull(actual.Content);
        }
        [Test]
        public void GetAllParentTasks_Test()
        {            
            TaskController tc = new TaskController();
            var result = tc.GetAllParentTasks();
            var actual = result as OkNegotiatedContentResult<List<ParentTaskModel>>;
            Assert.IsNotNull(actual);
            Assert.IsNotNull(actual.Content);
        }

        [Test]
        public void AddParentTasksDetail_Test()
        {
            ParentTaskModel parentTaskDetails = new ParentTaskModel();
            //parentTaskDetails.PARENT_ID = 1;
            parentTaskDetails.PARENT_TASK = "Parent12345";
            TaskController tc = new TaskController();
            IHttpActionResult result = tc.AddParentTasksDetail(parentTaskDetails);
            var actual = result as OkNegotiatedContentResult<ParentTaskModel>;
            Assert.IsNotNull(actual);
            Assert.IsNotNull(actual.Content);   
        }



    }

}
