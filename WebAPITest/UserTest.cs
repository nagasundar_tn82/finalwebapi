﻿using System;
using Web.API.Controllers;
using ModelLayer;
using System.Web.Http.Results;
using System.Collections.Generic;
using System.Web.Http;
using NUnit.Framework;

namespace WebAPITest
{
    [TestFixture]
    public class UserTest
    {
        [Test]
        public void GetAllUserDetails_Test()
        {
            UserController tc = new UserController();
            var result = tc.GetAllUserDetails();
            var actual = result as OkNegotiatedContentResult<List<UserModel>>;
            Assert.IsNotNull(actual);
            Assert.IsNotNull(actual.Content);
        }


        [Test]
        public void AddUserDetails_Test()      
        {
            UserModel UserDetails = new UserModel();
            //UserDetails.END_DATE = "01/01/2012";
            //UserDetails.PRIORITY = 1;
            //UserDetails.PROJECT = "AGI -API";
            //UserDetails.PROJECT_ID = 2;
            //UserDetails.START_DATE = "01/01/2012";
           // UserModel UserDetails = new UserModel();
            UserDetails.FIRST_NAME = "User1";
            UserDetails.EMPLOYEE_ID = 5;
            UserDetails.PROJECT_ID = 4;
            UserDetails.LAST_NAME = "LastName1";
            UserDetails.TASK_ID = 14;
            UserDetails.USER_ID = 1;
            UserController tc = new UserController();

            IHttpActionResult result = tc.AddUserDetails(UserDetails);
            var actual = result as OkNegotiatedContentResult<UserModel>;
            Assert.IsNotNull(actual);
            Assert.IsNotNull(actual.Content);
        }

        [Test]
        public void UpdateUserDetails_Test()
        {
            UserController tc = new UserController();
            UserModel UserDetails = new UserModel();
            UserDetails.FIRST_NAME = "User1";
            UserDetails.EMPLOYEE_ID = 5;
            UserDetails.PROJECT_ID = 4;
            UserDetails.LAST_NAME = "LastName1";
            UserDetails.TASK_ID = 14;
            UserDetails.USER_ID = 1;
            var result = tc.UpdateUserDetails(UserDetails);
            var actual = result as OkNegotiatedContentResult<UserModel>;
            Assert.IsNotNull(actual);
        }
        [Test]
        public void DeleteUserDetails_Test()
        {
            UserController tc = new UserController();
            var result = tc.DeleteUserDetails(1002);
            var actual = result as OkNegotiatedContentResult<bool>;
            Assert.IsNotNull(actual);
            Assert.IsNotNull(actual.Content);
        }

    }
}
