﻿using System;
using Web.API.Controllers;
using ModelLayer;
using System.Web.Http.Results;
using System.Collections.Generic;
using System.Web.Http;

using NUnit.Framework;

namespace WebAPITest
{
    [TestFixture]
    public class ProjectTest
    {
        [Test]
        public void GetAllProjectDetails_Test()
        {
            ProjectController tc = new ProjectController();
            var result = tc.GetAllProjectDetails();
            var actual = result as OkNegotiatedContentResult<List<ProjectModel>>;
            Assert.IsNotNull(actual);
            Assert.IsNotNull(actual.Content);
        } 
       

        [Test]
        public void AddProjectDetails_Test()
        // public IHttpActionResult AddProjectDetails()
        {
            ProjectModel projectDetails = new ProjectModel();
            projectDetails.PROJECT = "Proj1";
            projectDetails.PROJECT_ID = 5;
            projectDetails.PRIORITY = 4;
            projectDetails.START_DATE = "2019-15-4";
            projectDetails.END_DATE = "2019-18-5";
            ProjectController tc = new ProjectController();

            IHttpActionResult result = tc.AddProjectDetails(projectDetails);
            var actual = result as OkNegotiatedContentResult<ProjectModel>;
            Assert.IsNotNull(actual);
            Assert.IsNotNull(actual.Content);
        }

        [Test]
        public void UpdateProjectDetails_Test()
        {
            ProjectModel projectDetails = new ProjectModel();
            ProjectController tc = new ProjectController();
            projectDetails.PROJECT = "Proj1";
            projectDetails.PROJECT_ID = 1003;
            projectDetails.PRIORITY = 4;           
            projectDetails.START_DATE = "2019-15-4";
            projectDetails.END_DATE = "2019-18-5"; 
            var result = tc.UpdateProjectDetails(projectDetails);
            var actual = result as OkNegotiatedContentResult<ProjectModel>;
            Assert.IsNotNull(actual);
        }
        [Test]
        public void DeleteProjectDetails_Test()
        {
            ProjectController tc = new ProjectController();
            var result = tc.DeleteProjectDetails(1024);
            var actual = result as OkNegotiatedContentResult<bool>;
            Assert.IsNotNull(actual);
            Assert.IsNotNull(actual.Content);
        }

    }
}
